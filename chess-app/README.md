
# Chess.tsx

Chess game made with react.org documentation. 




## Under the hood

This project is using:

- React
- TypeScript
- OOP approach

## To do list

Future plans:

- To add any linter or prettier
- To refactor some stages by adding side libraries
- To style the desk and menu, improve a poor UI


